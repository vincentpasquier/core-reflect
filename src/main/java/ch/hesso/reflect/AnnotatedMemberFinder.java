/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.reflect;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class AnnotatedMemberFinder {

	// SLF4J Logger
	private static final Logger LOG = LoggerFactory.getLogger ( AnnotatedMemberFinder.class );

	public static <T extends Annotation> Map<Field, T> findAnnotatedMember ( final Class<?> $class, final Class<T> $annotation ) {
		Map<Field, T> fields = new HashMap<> ();

		outer:
		for ( Field field : $class.getDeclaredFields () ) {
			for ( Annotation annotation : field.getAnnotations () ) {
				if ( $annotation.isInstance ( annotation ) ) {
					fields.put ( field, $annotation.cast ( annotation ) );
					try {
						field.setAccessible ( true );
					} catch ( Exception e ) {
						LOG.debug ( "Normal exception received, field is now accessible" );
						// It is normal to get errors while changing method accessibility
					}
					continue outer;
				}
			}
		}

		return fields;
	}


	public static <T extends Annotation> Map<Method, T> findAnnotatedMethod ( final Class<?> $class, final Class<T> $annotation ) {
		Map<Method, T> methods = new HashMap<> ();

		outer:
		for ( Method method : $class.getDeclaredMethods () ) {
			for ( Annotation annotation : method.getAnnotations () ) {
				if ( $annotation.isInstance ( annotation ) ) {
					methods.put ( method, $annotation.cast ( annotation ) );
					try {
						method.setAccessible ( true );
					} catch ( Exception e ) {
						LOG.debug ( "Normal exception received, method is now accessible" );
						// It is normal to get errors while changing method accessibility
					}
					continue outer;
				}
			}
		}

		return methods;
	}


}
