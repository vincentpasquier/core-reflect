/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.reflect;


import com.google.common.collect.ImmutableSet;
import com.google.common.reflect.ClassPath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Modifier;
import java.util.HashSet;
import java.util.Set;

/**
 * Reflection utilities used to finds classes, subclasses and annotated classes.
 * <p/>
 * This class is parametrized to ensure compiler checks.
 *
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public final class TopLevelClassFinder<C> {

	// Logger java
	private static final Logger LOG = LoggerFactory.getLogger ( TopLevelClassFinder.class.getName () );

	//
	private ClassPath _classPath;

	/**
	 * Initializes the top level class finder, loads all information about classpath.
	 */
	public TopLevelClassFinder () {
		try {
			_classPath = ClassPath.from ( Thread.currentThread ().getContextClassLoader () );
		} catch ( IOException e ) {
			LOG.error ( "Something went wrong when reading classpath resources", e );
		}
	}

	/**
	 * Finds all class and subclasses derived from Class.
	 * <p/>
	 * Warning: The included classes can be abstract.
	 *
	 * @param $class     the top level class
	 * @param topPackage where to find them
	 *
	 * @return all class and subclasses from specified Class
	 */
	@SuppressWarnings ("unchecked")
	public ImmutableSet<Class<? extends C>> findClasses ( final Class<C> $class, final String topPackage ) {
		ImmutableSet<ClassPath.ClassInfo> $classes = _classPath.getTopLevelClassesRecursive ( topPackage );
		Set<Class<? extends C>> toReturn = new HashSet<> ();
		for ( ClassPath.ClassInfo $info : $classes ) {
			try {
				Class<?> $current = $info.load ();
				if ( $class.isAssignableFrom ( $current ) ) {
					// If it is assignable from class, it extends or implements it
					// Or it is exactly the $class provided
					toReturn.add ( (Class<? extends C>) $current );
				}
			} catch ( final NoClassDefFoundError e ) {
				//LOG.debug ( "A class was not found in the ClassPath, might be nothing!", e );
				// Nothing to worry about, most likely some Groovy or Scala not accessible with this method
			}
		}
		return ImmutableSet.copyOf ( toReturn );
	}

	/**
	 * Finds all subclasses from specified Class file.
	 * <p/>
	 * Warning: The included classes can be abstract.
	 *
	 * @param $class     the top level class
	 * @param topPackage where to find them
	 *
	 * @return all subclasses from specified Class
	 */
	public ImmutableSet<Class<? extends C>> findSubClasses ( final Class<C> $class,
																													 final String topPackage ) {
		Set<Class<? extends C>> $classes = new HashSet<> ( findClasses ( $class, topPackage ) );
		$classes.remove ( $class );
		return ImmutableSet.copyOf ( $classes );
	}

	/**
	 * @param $class
	 * @param topPackage
	 *
	 * @return
	 */
	public ImmutableSet<Class<? extends C>> findInstantiableSubClasses ( final Class<C> $class,
																														 final String topPackage ) {
		Set<Class<? extends C>> $classes = new HashSet<> ();
		for ( Class<? extends C> $current : findSubClasses ( $class, topPackage ) ) {
			boolean instantiable = true;
			instantiable &= !Modifier.isAbstract ( $current.getModifiers () );
			instantiable &= !Modifier.isInterface ( $current.getModifiers () );
			if ( instantiable ) {
				$classes.add ( $current );
			}
		}
		return ImmutableSet.copyOf ( $classes );
	}

	/**
	 * Finds all classes annotated with specified Annotation.
	 * <p/>
	 * Warning: The included classes can be abstract.
	 *
	 * @param annotation   the annotation class to find
	 * @param topDirectory where to find them
	 *
	 * @return all classes annotated with specified annotation
	 */
	public ImmutableSet<Class<?>> findAnnotatedClasses ( final Class<C> annotation,
																											 final String topDirectory ) {
		ImmutableSet<ClassPath.ClassInfo> $classes = _classPath.getTopLevelClassesRecursive ( topDirectory );
		Set<Class<?>> toReturn = new HashSet<> ();
		for ( ClassPath.ClassInfo $info : $classes ) {
			System.out.println ( $info.getName () );
			try {
				Class<?> $current = $info.load ();
				for ( Annotation $annotation : $current.getAnnotations () ) {
					boolean found = annotation.isInstance ( $annotation );
					if ( found ) {
						toReturn.add ( $current );
						break;
					}
				}
			} catch ( final NoClassDefFoundError e ) {
				//LOG.debug ( "A class was not found in the ClassPath, might be nothing!", e );
				// Nothing to worry about, most likely some Groovy or Scala not accessible with this method
			}
		}
		return ImmutableSet.copyOf ( toReturn );
	}
}