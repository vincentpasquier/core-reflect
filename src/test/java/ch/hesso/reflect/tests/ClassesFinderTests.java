/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.reflect.tests;

import ch.hesso.reflect.TopLevelClassFinder;
import ch.hesso.reflect.tests.dummies.*;
import com.google.common.collect.ImmutableSet;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class ClassesFinderTests {

	@Test
	public void testFindClassByPackage () {
		TopLevelClassFinder<SupremeClass> finder = new TopLevelClassFinder<> ();
		ImmutableSet<Class<? extends SupremeClass>> classes =
				finder.findClasses ( SupremeClass.class, SupremeClass.class.getPackage ().getName () );
		Assert.assertTrue ( classes.contains ( SupremeClass.class ) );
		Assert.assertTrue ( classes.contains ( SpawnOfSupremeClass.class ) );
		Assert.assertTrue ( !classes.contains ( AnnotatedSpawnOfSupremeInterface.class ) );
		Assert.assertTrue (
				!classes.contains ( AnnotatedSpawnOfSupremeInterface.InnerAnnotatedSpawnOfSpecialSupremeInterface.class )
		);
		Assert.assertTrue ( !classes.contains ( MagicClassAnnotation.class ) );
		Assert.assertTrue ( !classes.contains ( SpawnOfSpecialSupremeInterface.class ) );
		Assert.assertTrue ( !classes.contains ( SpecialSupremeInterface.class ) );
		Assert.assertTrue ( !classes.contains ( SupremeInterface.class ) );
	}

	@Test
	public void testFindSubClassesByPackage () {
		TopLevelClassFinder<SupremeClass> finder = new TopLevelClassFinder<> ();
		ImmutableSet<Class<? extends SupremeClass>> classes =
				finder.findSubClasses ( SupremeClass.class, SupremeClass.class.getPackage ().getName () );
		Assert.assertTrue ( classes.contains ( SpawnOfSupremeClass.class ) );
		Assert.assertTrue ( !classes.contains ( SupremeClass.class ) );
		Assert.assertTrue ( !classes.contains ( AnnotatedSpawnOfSupremeInterface.class ) );
		Assert.assertTrue (
				!classes.contains ( AnnotatedSpawnOfSupremeInterface.InnerAnnotatedSpawnOfSpecialSupremeInterface.class )
		);
		Assert.assertTrue ( !classes.contains ( MagicClassAnnotation.class ) );
		Assert.assertTrue ( !classes.contains ( SpawnOfSpecialSupremeInterface.class ) );
		Assert.assertTrue ( !classes.contains ( SpecialSupremeInterface.class ) );
		Assert.assertTrue ( !classes.contains ( SupremeInterface.class ) );
	}

	@Test
	public void testFindSubInterfacesByPackage () {
		TopLevelClassFinder<SupremeInterface> finder = new TopLevelClassFinder<> ();
		ImmutableSet<Class<? extends SupremeInterface>> classes =
				finder.findSubClasses ( SupremeInterface.class, SupremeInterface.class.getPackage ().getName () );
		Assert.assertTrue ( classes.contains ( AnnotatedSpawnOfSupremeInterface.class ) );
		Assert.assertTrue ( classes.contains ( SpawnOfSpecialSupremeInterface.class ) );
		Assert.assertTrue ( classes.contains ( SpecialSupremeInterface.class ) );
		Assert.assertTrue (
				!classes.contains ( AnnotatedSpawnOfSupremeInterface.InnerAnnotatedSpawnOfSpecialSupremeInterface.class )
		);
		Assert.assertTrue ( !classes.contains ( SpawnOfSupremeClass.class ) );
		Assert.assertTrue ( !classes.contains ( SupremeClass.class ) );
		Assert.assertTrue ( !classes.contains ( MagicClassAnnotation.class ) );
		Assert.assertTrue ( !classes.contains ( SupremeInterface.class ) );
	}

	@Test
	public void testFindClassByAnnotation () {
		TopLevelClassFinder<MagicClassAnnotation> finder = new TopLevelClassFinder<> ();
		ImmutableSet<Class<?>> classes =
				finder.findAnnotatedClasses ( MagicClassAnnotation.class, MagicClassAnnotation.class.getPackage ().getName () );
		Assert.assertTrue ( classes.contains ( AnnotatedSpawnOfSupremeInterface.class ) );
		Assert.assertTrue ( !classes.contains ( SpawnOfSpecialSupremeInterface.class ) );
		Assert.assertTrue ( !classes.contains ( SpecialSupremeInterface.class ) );
		Assert.assertTrue (
				!classes.contains ( AnnotatedSpawnOfSupremeInterface.InnerAnnotatedSpawnOfSpecialSupremeInterface.class )
		);
		Assert.assertTrue ( !classes.contains ( SpawnOfSupremeClass.class ) );
		Assert.assertTrue ( !classes.contains ( SupremeClass.class ) );
		Assert.assertTrue ( !classes.contains ( MagicClassAnnotation.class ) );
		Assert.assertTrue ( !classes.contains ( SupremeInterface.class ) );
	}

	@Test
	public void testFailClassByUnknownPackage () {
		TopLevelClassFinder<SupremeClass> finder = new TopLevelClassFinder<> ();
		ImmutableSet<Class<? extends SupremeClass>> classes =
				finder.findClasses ( SupremeClass.class, "com" );
		Assert.assertTrue ( classes.size () == 0 );
	}
}
