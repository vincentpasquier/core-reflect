/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.reflect.tests;

import ch.hesso.reflect.AnnotatedMemberFinder;
import ch.hesso.reflect.tests.dummies.MegaAnnotatedClass;
import ch.hesso.reflect.tests.dummies.MegaMemberAnnotation;
import ch.hesso.reflect.tests.dummies.MegaMethodAnnotation;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class AnnotatedFinderTests {

	@Test
	public void testMemberAnnotation () {
		Map<Field, MegaMemberAnnotation> annotations
				= AnnotatedMemberFinder.findAnnotatedMember (
				MegaAnnotatedClass.class,
				MegaMemberAnnotation.class
		);

		Assert.assertTrue ( annotations.size () == 2 );
	}

	@Test
	public void testMethodAnnotation () {
		Map<Method, MegaMethodAnnotation> annotations
				= AnnotatedMemberFinder.findAnnotatedMethod (
				MegaAnnotatedClass.class,
				MegaMethodAnnotation.class
		);

		Assert.assertTrue ( annotations.size () == 1 );
	}

}
